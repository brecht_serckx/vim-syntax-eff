" Vim syntax file
" Language:     eff
" Maintainer:   Brecht Serckx
" Latest Revision: Oct 22 2018

" quit when a syntax file was already loaded
if exists("b:current_syntax") && b:current_syntax == "eff"
  finish
endif

" OCaml is case sensitive.
syn case match

" Access to the method of an object
syn match    effMethod       "#"

" Script headers highlighted like comments
syn match    effComment   "^#!.*" contains=@Spell

" Scripting directives
syn match    effScript "^#\<\(quit\|labels\|warnings\|directory\|cd\|load\|use\|install_printer\|remove_printer\|require\|thread\|trace\|untrace\|untrace_all\|print_depth\|print_length\|camlp4o\)\>"

" lowercase identifier - the standard way to match
syn match    effLCIdentifier /\<\(\l\|_\)\(\w\|'\)*\>/

syn match    effKeyChar    "|"

" Errors
syn match    effBraceErr   "}"
syn match    effBrackErr   "\]"
syn match    effParenErr   ")"
syn match    effArrErr     "|]"

syn match    effCommentErr "\*)"

syn match    effCountErr   "\<downto\>"
syn match    effCountErr   "\<to\>"

if !exists("eff_revised")
  syn match    effDoErr      "\<do\>"
endif

syn match    effDoneErr    "\<done\>"
syn match    effThenErr    "\<then\>"

" Error-highlighting of "end" without synchronization:
" as keyword or as error (default)
if exists("eff_noend_error")
  syn match    effKeyword    "\<end\>"
else
  syn match    effEndErr     "\<end\>"
endif

" Some convenient clusters
syn cluster  effAllErrs contains=effBraceErr,effBrackErr,effParenErr,effCommentErr,effCountErr,effDoErr,effDoneErr,effEndErr,effThenErr

syn cluster  effAENoParen contains=effBraceErr,effBrackErr,effCommentErr,effCountErr,effDoErr,effDoneErr,effEndErr,effThenErr

syn cluster  effContained contains=effTodo,effPreDef,effModParam,effModParam1,effPreMPRestr,effMPRestr,effMPRestr1,effMPRestr2,effMPRestr3,effModRHS,effFuncWith,effFuncStruct,effModTypeRestr,effModTRWith,effWith,effWithRest,effModType,effFullMod,effVal


" Enclosing delimiters
syn region   effEncl transparent matchgroup=effKeyword start="(" matchgroup=effKeyword end=")" contains=ALLBUT,@effContained,effParenErr
syn region   effEncl transparent matchgroup=effKeyword start="{" matchgroup=effKeyword end="}"  contains=ALLBUT,@effContained,effBraceErr
syn region   effEncl transparent matchgroup=effKeyword start="\[" matchgroup=effKeyword end="\]" contains=ALLBUT,@effContained,effBrackErr
syn region   effEncl transparent matchgroup=effKeyword start="\[|" matchgroup=effKeyword end="|\]" contains=ALLBUT,@effContained,effArrErr


" Comments
syn region   effComment start="(\*" end="\*)" contains=@Spell,effComment,effTodo
syn keyword  effTodo contained TODO FIXME XXX NOTE


" Objects
syn region   effEnd matchgroup=effObject start="\<object\>" matchgroup=effObject end="\<end\>" contains=ALLBUT,@effContained,effEndErr


" Blocks
if !exists("eff_revised")
  syn region   effEnd matchgroup=effKeyword start="\<begin\>" matchgroup=effKeyword end="\<end\>" contains=ALLBUT,@effContained,effEndErr
endif


" "for"
syn region   effNone matchgroup=effKeyword start="\<for\>" matchgroup=effKeyword end="\<\(to\|downto\)\>" contains=ALLBUT,@effContained,effCountErr


" "do"
if !exists("eff_revised")
  syn region   effDo matchgroup=effKeyword start="\<do\>" matchgroup=effKeyword end="\<done\>" contains=ALLBUT,@effContained,effDoneErr
endif

" "if"
syn region   effNone matchgroup=effKeyword start="\<if\>" matchgroup=effKeyword end="\<then\>" contains=ALLBUT,@effContained,effThenErr


"" Modules

" "sig"
syn region   effSig matchgroup=effModule start="\<sig\>" matchgroup=effModule end="\<end\>" contains=ALLBUT,@effContained,effEndErr,effModule
syn region   effModSpec matchgroup=effKeyword start="\<module\>" matchgroup=effModule end="\<\u\(\w\|'\)*\>" contained contains=@effAllErrs,effComment skipwhite skipempty nextgroup=effModTRWith,effMPRestr

" "open"
syn region   effNone matchgroup=effKeyword start="\<open\>" matchgroup=effModule end="\<\u\(\w\|'\)*\( *\. *\u\(\w\|'\)*\)*\>" contains=@effAllErrs,effComment

" "include"
syn match    effKeyword "\<include\>" skipwhite skipempty nextgroup=effModParam,effFullMod

" "module" - somewhat complicated stuff ;-)
syn region   effModule matchgroup=effKeyword start="\<module\>" matchgroup=effModule end="\<\u\(\w\|'\)*\>" contains=@effAllErrs,effComment skipwhite skipempty nextgroup=effPreDef
syn region   effPreDef start="."me=e-1 matchgroup=effKeyword end="\l\|=\|)"me=e-1 contained contains=@effAllErrs,effComment,effModParam,effModTypeRestr,effModTRWith nextgroup=effModPreRHS
syn region   effModParam start="([^*]" end=")" contained contains=@effAENoParen,effModParam1,effVal
syn match    effModParam1 "\<\u\(\w\|'\)*\>" contained skipwhite skipempty nextgroup=effPreMPRestr

syn region   effPreMPRestr start="."me=e-1 end=")"me=e-1 contained contains=@effAllErrs,effComment,effMPRestr,effModTypeRestr

syn region   effMPRestr start=":" end="."me=e-1 contained contains=@effComment skipwhite skipempty nextgroup=effMPRestr1,effMPRestr2,effMPRestr3
syn region   effMPRestr1 matchgroup=effModule start="\ssig\s\=" matchgroup=effModule end="\<end\>" contained contains=ALLBUT,@effContained,effEndErr,effModule
syn region   effMPRestr2 start="\sfunctor\(\s\|(\)\="me=e-1 matchgroup=effKeyword end="->" contained contains=@effAllErrs,effComment,effModParam skipwhite skipempty nextgroup=effFuncWith,effMPRestr2
syn match    effMPRestr3 "\w\(\w\|'\)*\( *\. *\w\(\w\|'\)*\)*" contained
syn match    effModPreRHS "=" contained skipwhite skipempty nextgroup=effModParam,effFullMod
syn keyword  effKeyword val
syn region   effVal matchgroup=effKeyword start="\<val\>" matchgroup=effLCIdentifier end="\<\l\(\w\|'\)*\>" contains=@effAllErrs,effComment,effFullMod skipwhite skipempty nextgroup=effMPRestr
syn region   effModRHS start="." end=". *\w\|([^*]"me=e-2 contained contains=effComment skipwhite skipempty nextgroup=effModParam,effFullMod
syn match    effFullMod "\<\u\(\w\|'\)*\( *\. *\u\(\w\|'\)*\)*" contained skipwhite skipempty nextgroup=effFuncWith

syn region   effFuncWith start="([^*]"me=e-1 end=")" contained contains=effComment,effWith,effFuncStruct skipwhite skipempty nextgroup=effFuncWith
syn region   effFuncStruct matchgroup=effModule start="[^a-zA-Z]struct\>"hs=s+1 matchgroup=effModule end="\<end\>" contains=ALLBUT,@effContained,effEndErr

syn match    effModTypeRestr "\<\w\(\w\|'\)*\( *\. *\w\(\w\|'\)*\)*\>" contained
syn region   effModTRWith start=":\s*("hs=s+1 end=")" contained contains=@effAENoParen,effWith
syn match    effWith "\<\(\u\(\w\|'\)* *\. *\)*\w\(\w\|'\)*\>" contained skipwhite skipempty nextgroup=effWithRest
syn region   effWithRest start="[^)]" end=")"me=e-1 contained contains=ALLBUT,@effContained

" "struct"
syn region   effStruct matchgroup=effModule start="\<\(module\s\+\)\=struct\>" matchgroup=effModule end="\<end\>" contains=ALLBUT,@effContained,effEndErr

" "module type"
syn region   effKeyword start="\<module\>\s*\<type\>\(\s*\<of\>\)\=" matchgroup=effModule end="\<\w\(\w\|'\)*\>" contains=effComment skipwhite skipempty nextgroup=effMTDef
syn match    effMTDef "=\s*\w\(\w\|'\)*\>"hs=s+1,me=s+1 skipwhite skipempty nextgroup=effFullMod

syn keyword  effKeyword  and as assert class
syn keyword  effKeyword  constraint else
syn keyword  effKeyword  exception external fun

syn keyword  effKeyword  in inherit initializer
syn keyword  effKeyword  land lazy let match
syn keyword  effKeyword  method mutable new of
syn keyword  effKeyword  parser private raise rec
syn keyword  effKeyword  try type
syn keyword  effKeyword  virtual when while with

if exists("eff_revised")
  syn keyword  effKeyword  do value
  syn keyword  effBoolean  True False
else
  syn keyword  effKeyword  function
  syn keyword  effBoolean  true false
  syn match    effKeyChar  "!"
endif

syn keyword  effType     array bool char exn float format format4
syn keyword  effType     int int32 int64 lazy_t list nativeint option
syn keyword  effType     string unit

syn keyword  effOperator asr lnot lor lsl lsr lxor mod not

syn match    effConstructor  "(\s*)"
syn match    effConstructor  "\[\s*\]"
syn match    effConstructor  "\[|\s*>|]"
syn match    effConstructor  "\[<\s*>\]"
syn match    effConstructor  "\u\(\w\|'\)*\>"

" Polymorphic variants
syn match    effConstructor  "`\w\(\w\|'\)*\>"

" Module prefix
syn match    effModPath      "\u\(\w\|'\)* *\."he=e-1

syn match    effCharacter    "'\\\d\d\d'\|'\\[\'ntbr]'\|'.'"
syn match    effCharacter    "'\\x\x\x'"
syn match    effCharErr      "'\\\d\d'\|'\\\d'"
syn match    effCharErr      "'\\[^\'ntbr]'"
syn region   effString       start=+"+ skip=+\\\\\|\\"+ end=+"+ contains=@Spell

syn match    effFunDef       "->"
syn match    effRefAssign    ":="
syn match    effTopStop      ";;"
syn match    effOperator     "\^"
syn match    effOperator     "::"

syn match    effOperator     "&&"
syn match    effOperator     "<"
syn match    effOperator     ">"
syn match    effAnyVar       "\<_\>"
syn match    effKeyChar      "|[^\]]"me=e-1
syn match    effKeyChar      ";"
syn match    effKeyChar      "\~"
syn match    effKeyChar      "?"
syn match    effKeyChar      "\*"
syn match    effKeyChar      "="

if exists("eff_revised")
  syn match    effErr        "<-"
else
  syn match    effOperator   "<-"
endif

syn match    effNumber        "\<-\=\d\(_\|\d\)*[l|L|n]\?\>"
syn match    effNumber        "\<-\=0[x|X]\(\x\|_\)\+[l|L|n]\?\>"
syn match    effNumber        "\<-\=0[o|O]\(\o\|_\)\+[l|L|n]\?\>"
syn match    effNumber        "\<-\=0[b|B]\([01]\|_\)\+[l|L|n]\?\>"
syn match    effFloat         "\<-\=\d\(_\|\d\)*\.\?\(_\|\d\)*\([eE][-+]\=\d\(_\|\d\)*\)\=\>"

" Labels
syn match    effLabel        "\~\(\l\|_\)\(\w\|'\)*"lc=1
syn match    effLabel        "?\(\l\|_\)\(\w\|'\)*"lc=1
syn region   effLabel transparent matchgroup=effLabel start="?(\(\l\|_\)\(\w\|'\)*"lc=2 end=")"me=e-1 contains=ALLBUT,@effContained,effParenErr


" Synchronization
syn sync minlines=50
syn sync maxlines=500

if !exists("eff_revised")
  syn sync match effDoSync      grouphere  effDo      "\<do\>"
  syn sync match effDoSync      groupthere effDo      "\<done\>"
endif

if exists("eff_revised")
  syn sync match effEndSync     grouphere  effEnd     "\<\(object\)\>"
else
  syn sync match effEndSync     grouphere  effEnd     "\<\(begin\|object\)\>"
endif

syn sync match effEndSync     groupthere effEnd     "\<end\>"
syn sync match effStructSync  grouphere  effStruct  "\<struct\>"
syn sync match effStructSync  groupthere effStruct  "\<end\>"
syn sync match effSigSync     grouphere  effSig     "\<sig\>"
syn sync match effSigSync     groupthere effSig     "\<end\>"

" Define the default highlighting.
" Only when an item doesn't have highlighting yet

hi def link effBraceErr	   Error
hi def link effBrackErr	   Error
hi def link effParenErr	   Error
hi def link effArrErr	   Error

hi def link effCommentErr   Error

hi def link effCountErr	   Error
hi def link effDoErr	   Error
hi def link effDoneErr	   Error
hi def link effEndErr	   Error
hi def link effThenErr	   Error

hi def link effCharErr	   Error

hi def link effErr	   Error

hi def link effComment	   Comment

hi def link effModPath	   Include
hi def link effObject	   Include
hi def link effModule	   Include
hi def link effModParam1    Include
hi def link effModType	   Include
hi def link effMPRestr3	   Include
hi def link effFullMod	   Include
hi def link effModTypeRestr Include
hi def link effWith	   Include
hi def link effMTDef	   Include

hi def link effScript	   Include

hi def link effConstructor  Constant

hi def link effVal          Keyword
hi def link effModPreRHS    Keyword
hi def link effMPRestr2	   Keyword
hi def link effKeyword	   Keyword
hi def link effMethod	   Include
hi def link effFunDef	   Keyword
hi def link effRefAssign    Keyword
hi def link effKeyChar	   Keyword
hi def link effAnyVar	   Keyword
hi def link effTopStop	   Keyword
hi def link effOperator	   Keyword

hi def link effBoolean	   Boolean
hi def link effCharacter    Character
hi def link effNumber	   Number
hi def link effFloat	   Float
hi def link effString	   String

hi def link effLabel	   Identifier

hi def link effType	   Type

hi def link effTodo	   Todo

hi def link effEncl	   Keyword


let b:current_syntax = "eff"

" vim: ts=8
